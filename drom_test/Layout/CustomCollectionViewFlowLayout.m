//
//  CustomCollectionViewFlowLayout.m
//  drom_test
//
//  Created by Роман Штро on 15/12/2018.
//  Copyright © 2018 Роман Штро. All rights reserved.
//

#import "CustomCollectionViewFlowLayout.h"

@interface CustomCollectionViewFlowLayout ()

@property (strong, nonatomic) NSMutableArray<NSIndexPath *> *removingIndexPaths;

@end

@implementation CustomCollectionViewFlowLayout

- (instancetype)init {
    self = [super init];
    if (self) {
        _removingIndexPaths = [[NSMutableArray alloc] init];
    }
    return self;
}

- (CGSize)itemSize {
    if (self.collectionView) {
        UIEdgeInsets edgeInsets = [self safeArea];
        edgeInsets.left += self.sectionInset.left;
        edgeInsets.right += self.sectionInset.right;
        float width = CGRectGetWidth(self.collectionView.bounds) - edgeInsets.left - edgeInsets.right;
        return CGSizeMake(width, width);
    } else {
        return [super itemSize];
    }
}

- (UIEdgeInsets)safeArea {
    UIEdgeInsets edgeInsets = UIEdgeInsetsMake(0.f, 0.f, 0.f, 0.f);
    if (@available(iOS 11.0, *)) {
        if (self.collectionView) {
            edgeInsets = self.collectionView.safeAreaInsets;
        }
    }
    return edgeInsets;
}

- (void)prepareForCollectionViewUpdates:(NSArray<UICollectionViewUpdateItem *> *)updateItems {
    [super prepareForCollectionViewUpdates:updateItems];
    
    [self.removingIndexPaths removeAllObjects];
    
    for (UICollectionViewUpdateItem *collectionViewUpdateItem in updateItems) {
        if (collectionViewUpdateItem.updateAction == UICollectionUpdateActionDelete) {
            [self.removingIndexPaths addObject:collectionViewUpdateItem.indexPathBeforeUpdate];
        }
    }
}

- (UICollectionViewLayoutAttributes *)finalLayoutAttributesForDisappearingItemAtIndexPath:(NSIndexPath *)itemIndexPath {
    UICollectionViewLayoutAttributes *attributes = [super finalLayoutAttributesForDisappearingItemAtIndexPath:itemIndexPath];
    if (self.removingIndexPaths.count > 0) {
        if ([self.removingIndexPaths containsObject:itemIndexPath]) {
            attributes.alpha = 1.f;
            attributes.transform3D = CATransform3DMakeTranslation(self.collectionView.bounds.size.width, 0.f, 0.f);
        }
    }
    return attributes;
}

- (void)finalizeCollectionViewUpdates {
    [super finalizeCollectionViewUpdates];
    
    [self.removingIndexPaths removeAllObjects];
}

@end
