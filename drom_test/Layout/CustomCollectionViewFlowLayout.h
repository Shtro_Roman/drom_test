//
//  CustomCollectionViewFlowLayout.h
//  drom_test
//
//  Created by Роман Штро on 15/12/2018.
//  Copyright © 2018 Роман Штро. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCollectionViewFlowLayout : UICollectionViewFlowLayout

@end
