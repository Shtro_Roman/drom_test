//
//  ServerImageObject.m
//  drom_test
//
//  Created by Роман Штро on 12/12/2018.
//  Copyright © 2018 Роман Штро. All rights reserved.
//

#import "ServerImageObject.h"

@interface ServerImageObject ()

@property (strong,nonatomic) NSURL *imageUrl;
@property (strong,nonatomic) NSURLSessionDataTask *imageTask;

@end

@implementation ServerImageObject

- (instancetype)initWithImageUrl:(NSURL *)imageUrl {
    self = [super init];
    if (self) {
        _imageUrl = imageUrl;
    }
    return self;
}

#pragma mark - Public

- (UIImage *)image {
    NSString *localImagePath = [self localImagePath];
    if ([[NSFileManager defaultManager] fileExistsAtPath:localImagePath]) {
        NSData *localImageData = [NSData dataWithContentsOfFile:localImagePath];
        return [UIImage imageWithData:localImageData];
    }
    
    return nil;
}

- (void)startDownloadImage {
    if ([self image]) {
        [self.delegate serverImageObject:self didFinishDownloadImageWithError:nil];
    } else {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self tryDownloadImage];
        });
    }
}


- (void)suspendDownloadImage {
    if (self.imageTask) {
        [self.imageTask suspend];
    }
}

- (void)cancelDownloadImage {
    if (self.imageTask) {
        [self.imageTask cancel];
        self.imageTask = nil;
    }
}

- (void)clearAllData {
    [self cancelDownloadImage];
    
    NSString *localImagePath = [self localImagePath];
    if ([[NSFileManager defaultManager] fileExistsAtPath:localImagePath]) {
        [[NSFileManager defaultManager] removeItemAtPath:localImagePath error:nil];
    }
}

#pragma mark - Private

- (void)tryDownloadImage {
    if (!self.imageTask) {
        self.imageTask = [[NSURLSession sharedSession] dataTaskWithURL:self.imageUrl
                                                     completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                                                         [self imageTaskCompletedWithData:data response:response error:error];
                                                     }];
        [self.imageTask resume];
    } else {
        switch (self.imageTask.state) {
            case NSURLSessionTaskStateSuspended:
                [self.imageTask resume];
                break;
                
            default:
                break;
        }
    }
}

- (void)imageTaskCompletedWithData:(NSData *)data response:(NSURLResponse *)response error:(NSError *)error {
    if (error) {
        [self.delegate serverImageObject:self didFinishDownloadImageWithError:error];
    } else {
        NSString *localImagePath = [self localImagePath];
        if ([[NSFileManager defaultManager] fileExistsAtPath:localImagePath]) {
            [[NSFileManager defaultManager] removeItemAtPath:localImagePath
                                                       error:nil];
        }
        
        [data writeToFile:localImagePath atomically:YES];
        [self.delegate serverImageObject:self didFinishDownloadImageWithError:nil];
    }
    
    self.imageTask = nil;
}

- (NSString *)localImagePath {
    NSString *localImagePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
    localImagePath = [localImagePath stringByAppendingPathComponent:self.imageUrl.lastPathComponent];
    return localImagePath;
}

@end
