//
//  DataSource.m
//  drom_test
//
//  Created by Роман Штро on 12/12/2018.
//  Copyright © 2018 Роман Штро. All rights reserved.
//

#import "DataSource.h"

@interface DataSource ()

@property (strong,nonatomic) NSMutableArray<ServerImageObject *> *serverImageObjectsArray;

@end

@implementation DataSource

- (instancetype)init {
    self = [super init];
    if (self) {
        _serverImageObjectsArray = [[NSMutableArray alloc] init];
        [_serverImageObjectsArray addObjectsFromArray:[self createServerImageObjects]];
    }
    return self;
}

#pragma mark - Public

- (NSUInteger)countOfServerImageObjects {
    return self.serverImageObjectsArray.count;
}

- (ServerImageObject *)serverImageObjectAtIndex:(NSUInteger)index {
    if (index < [self countOfServerImageObjects]) {
        return [self.serverImageObjectsArray objectAtIndex:index];
    }
    return nil;
}

- (NSUInteger)indexOfServerImageObject:(ServerImageObject *)serverImageObject {
    return [self.serverImageObjectsArray indexOfObject:serverImageObject];
}

- (void)removeServerImageObjectAtIndex:(NSUInteger)index {
    if (index < [self countOfServerImageObjects]) {
        ServerImageObject *serverImageObject = [self.serverImageObjectsArray objectAtIndex:index];
        [serverImageObject clearAllData];
        [self.serverImageObjectsArray removeObjectAtIndex:index];
    }
}

- (void)reloadData {
    for (ServerImageObject *serverImageObject in self.serverImageObjectsArray) {
        [serverImageObject clearAllData];
    }
    [self.serverImageObjectsArray removeAllObjects];
    [self.serverImageObjectsArray addObjectsFromArray:[self createServerImageObjects]];
}

#pragma mark - Private

- (NSMutableArray<ServerImageObject *> *)createServerImageObjects {
    NSMutableArray<ServerImageObject *> *serverImageObjectsArray = [[NSMutableArray alloc] init];
    NSArray<NSString *> *imageStringUrlsArray = @[
                                                  @"https://i.ytimg.com/vi/4i4bh4OZwzg/maxresdefault.jpg",
                                                  @"https://bipbap.ru/wp-content/uploads/2017/04/187604chan1309313071950.jpg",
                                                  @"https://ribalych.ru/wp-content/uploads/2018/03/krasivye-foto_000.jpg",
                                                  @"https://2krota.ru/wp-content/uploads/2018/10/5a47f14eb667f_.jpg.8024091ab9d0d7b948f7d349d740d5c9.jpg",
                                                  @"https://img.tsn.ua/cached/1533905774/tsn-db9ef401efc93a5fd1b676cd38abbef3/thumbs/1200x630/20/f3/61b59b7f9c8d62ffbda99d6a0481f320.png",
                                                  @"https://bigpicture.ru/wp-content/uploads/2018/01/Majestic_beauty_of_winter_in_Japan3.jpg"
                                                  ];
    for (NSString *stringUrl in imageStringUrlsArray) {
        ServerImageObject *serverImageObject = [[ServerImageObject alloc] initWithImageUrl:[NSURL URLWithString:stringUrl]];
        [serverImageObjectsArray addObject:serverImageObject];
    }
    
    return serverImageObjectsArray;
}

@end
