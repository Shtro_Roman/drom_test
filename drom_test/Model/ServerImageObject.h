//
//  ServerImageObject.h
//  drom_test
//
//  Created by Роман Штро on 12/12/2018.
//  Copyright © 2018 Роман Штро. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class ServerImageObject;

@protocol ServerImageObjectDelegate <NSObject>

//if error == nil - success, else - fail.
- (void)serverImageObject:(ServerImageObject *)serverImageObject didFinishDownloadImageWithError:(NSError *)error;

@end

@interface ServerImageObject : NSObject

- (instancetype)initWithImageUrl:(NSURL *)imageUrl;

- (UIImage *)image;

- (void)startDownloadImage;
- (void)suspendDownloadImage;
- (void)cancelDownloadImage;

- (void)clearAllData;

@property (weak,nonatomic) id<ServerImageObjectDelegate>delegate;



- (instancetype) init __attribute__((unavailable("init not available")));
+ (instancetype) new __attribute__((unavailable("new not available")));

@end
