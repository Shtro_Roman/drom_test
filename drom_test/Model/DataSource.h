//
//  DataSource.h
//  drom_test
//
//  Created by Роман Штро on 12/12/2018.
//  Copyright © 2018 Роман Штро. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServerImageObject.h"

@interface DataSource : NSObject

- (NSUInteger)countOfServerImageObjects;
- (ServerImageObject *)serverImageObjectAtIndex:(NSUInteger)index;
- (NSUInteger)indexOfServerImageObject:(ServerImageObject *)serverImageObject;
- (void)removeServerImageObjectAtIndex:(NSUInteger)index;
- (void)reloadData;

@end
