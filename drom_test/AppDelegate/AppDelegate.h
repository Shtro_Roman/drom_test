//
//  AppDelegate.h
//  drom_test
//
//  Created by Роман Штро on 12/12/2018.
//  Copyright © 2018 Роман Штро. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

