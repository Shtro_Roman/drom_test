//
//  ImageCollectionViewCell.m
//  drom_test
//
//  Created by Роман Штро on 12/12/2018.
//  Copyright © 2018 Роман Штро. All rights reserved.
//

#import "ImageCollectionViewCell.h"

@interface ImageCollectionViewCell ()

@property (weak,nonatomic) UIImageView *imageView;

@end

@implementation ImageCollectionViewCell

#pragma mark - initialize
- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    self.layer.masksToBounds = YES;
    
    UIImageView *imageView = [[UIImageView alloc] init];
    [imageView setContentMode:UIViewContentModeScaleAspectFill];
    [self addSubview:imageView];
    self.imageView = imageView;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.imageView.frame = self.bounds;
}

#pragma mark - Public
- (void)setImage:(UIImage *)image {
    self.imageView.image = image;
}

@end
