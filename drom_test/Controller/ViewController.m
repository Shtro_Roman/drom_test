//
//  ViewController.m
//  drom_test
//
//  Created by Роман Штро on 12/12/2018.
//  Copyright © 2018 Роман Штро. All rights reserved.
//

#import "ViewController.h"
#import "ImageCollectionViewCell.h"
#import "DataSource.h"
#import "CustomCollectionViewFlowLayout.h"

@interface ViewController () <UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,ServerImageObjectDelegate>

@property (weak,nonatomic) UICollectionView *collectionView;
@property (weak,nonatomic) UIRefreshControl *refreshControl;
@property (strong,nonatomic) DataSource *dataSource;

@end

@implementation ViewController

CGFloat const padding = 10;
NSString *const cellIdentifier = @"cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.dataSource = [[DataSource alloc] init];
    
    CustomCollectionViewFlowLayout *layout = [[CustomCollectionViewFlowLayout alloc] init];
    layout.minimumInteritemSpacing = padding;
    layout.sectionInset = UIEdgeInsetsMake(padding, padding, padding, padding);
    
    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds
                                                          collectionViewLayout:layout];
    [collectionView registerClass:[ImageCollectionViewCell class] forCellWithReuseIdentifier:cellIdentifier];
    collectionView.backgroundColor = [UIColor lightGrayColor];
    collectionView.delegate = self;
    collectionView.dataSource = self;
    collectionView.alwaysBounceVertical = YES;
    [self.view addSubview:collectionView];
    self.collectionView = collectionView;
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor blackColor];
    [refreshControl addTarget:self action:@selector(refreshControlAction:) forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview:refreshControl];
    self.refreshControl = refreshControl;
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    self.collectionView.frame = CGRectMake(0.f,
                                           0.f,
                                           CGRectGetWidth(self.view.bounds),
                                           CGRectGetHeight(self.view.bounds));
    [self.collectionView.collectionViewLayout invalidateLayout];
}

- (void)refreshControlAction:(UIRefreshControl *)refreshControl {
    [self.dataSource reloadData];
    [self.collectionView reloadData];
    [refreshControl endRefreshing];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.dataSource countOfServerImageObjects];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.backgroundColor = [UIColor whiteColor];
    
    ServerImageObject *serverImageObject = [self.dataSource serverImageObjectAtIndex:indexPath.row];
    serverImageObject.delegate = self;
    [cell setImage:[serverImageObject image]];
    
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    ServerImageObject *serverImageObject = [self.dataSource serverImageObjectAtIndex:indexPath.row];
    if ([serverImageObject image]) {
        [self.collectionView performBatchUpdates:^{
            [self.dataSource removeServerImageObjectAtIndex:indexPath.row];
            [self.collectionView deleteItemsAtIndexPaths:@[indexPath]];
        } completion:nil];
    }
    return NO;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    ServerImageObject *serverImageObject = [self.dataSource serverImageObjectAtIndex:indexPath.row];
    if (![serverImageObject image]) {
        [serverImageObject startDownloadImage];
    }
}

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    ServerImageObject *serverImageObject = [self.dataSource serverImageObjectAtIndex:indexPath.row];
    if (![serverImageObject image]) {
        [serverImageObject suspendDownloadImage];
    }
}

#pragma mark - ServerImageObjectDelegate

- (void)serverImageObject:(ServerImageObject *)serverImageObject didFinishDownloadImageWithError:(NSError *)error {
    if (!error) {
        NSUInteger row = [self.dataSource indexOfServerImageObject:serverImageObject];
        if (row != NSNotFound) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.collectionView reloadItemsAtIndexPaths:@[indexPath]];
            });
        }
    } else {
        NSLog(@"%@",error.localizedDescription);
    }
}

@end
